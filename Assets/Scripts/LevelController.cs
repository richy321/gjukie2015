﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class LevelController : MonoBehaviour 
{
    public enum ScreenType
    {
        CharacterSelect,
        Conversation,
        //Briefcase
    }


	public List<string> maleSpeakNoises = new List<string> ();
    public ScreenType CurrentScreen;
    public Level CurrentLevel;
	public RectTransform speachBubble;

    public GameObject BriefCasePanel;
    public GameObject CharacterSelectPanel;
    public GameObject ConversationPanel;


	public bool textIsLeft=true;
	// Use this for initialization
	void Start () 
    {

        if(!CharacterSelectPanel)
            CharacterSelectPanel = GameObject.Find("ConvoCharacterSelect");
        if(!ConversationPanel)
            ConversationPanel = GameObject.Find("ConvoPanel");
        if(!BriefCasePanel)
            BriefCasePanel = GameObject.Find("DisguiseCaseLid");

        LoadLevel(CurrentLevel);
        SwitchScreen(CurrentScreen);
	}
	
	// Update is called once per frame
	void Update () 
    {
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();

		}

		if (textIsLeft == true) {
			Vector3 scale = speachBubble.localScale;
			scale.x = Mathf.Abs(scale.x);
			speachBubble.localScale = scale;
		} else {
			Vector3 scale = speachBubble.localScale;
			scale.x = -Mathf.Abs(scale.x);
			speachBubble.localScale = scale;
		}
	}

    public void Reset()
    {
        CurrentScreen = ScreenType.CharacterSelect;
    }

    public void LoadLevel(Level level)
    {
        Reset();
        level.Initialise();
        CurrentLevel = level;
    }

    public void LoadNextLevel()
    {
        if (CurrentLevel)
        {
            if (CurrentLevel.NextLevel)
            {
                LoadLevel(CurrentLevel.NextLevel);
            }
            else
            {
                //ENDGAME screen
            }
        }
    }

    public void SwitchScreen(ScreenType screen)
    {
        CharacterSelectPanel.SetActive(false);
        ConversationPanel.SetActive(false);
        
        switch (screen)
        {
            case ScreenType.CharacterSelect:
                CharacterSelectPanel.SetActive(true);
                break;
            case ScreenType.Conversation:
                ConversationPanel.SetActive(true);
                break;
        }
    }
}
