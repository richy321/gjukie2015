﻿using UnityEngine;
using System.Collections.Generic;


[System.Serializable]
public class ConvoNode :  MonoBehaviour 
{
	public enum SpeakerType
	{
		Player,
		ConvoPartner
	}

	public Disguise disguiseRequired;
	
	public bool endOfLevel = false;
    public bool decrementHealth = false;
	public ConvoNode jumpNode;
	[HideInInspector]
    public List<ConvoNode> children = new List<ConvoNode>();
    public Disguise unlockDisguise;

	public SpeakerType Speaker;
    [Multiline]
    public string Text = "Empty";
}