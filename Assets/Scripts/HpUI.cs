﻿using UnityEngine;
using System.Collections;

public class HpUI : MonoBehaviour {

	public PlayerController playerContr;
	int lastKnownHp;
	public GameObject heart1;
	public GameObject heart2;
	public GameObject heart3;

	// Use this for initialization
	void Start () {
		playerContr = GameObject.FindObjectOfType<PlayerController> ();
		lastKnownHp = playerContr.HP;
		heart1.SetActive (true);
		heart2.SetActive (true);
		heart3.SetActive (true);

	}
	
	// Update is called once per frame
	void Update () {
		if (lastKnownHp != playerContr.HP) {
			HpUi ();
		}
	}

	public void HpUi (){

		if (lastKnownHp == 2) {
			heart3.SetActive (false);
		} else if (lastKnownHp == 1) {
			heart2.SetActive (false);
			heart3.SetActive (false);
		} else if (lastKnownHp == 0) {
			heart1.SetActive (false);
			heart2.SetActive (false);
			heart3.SetActive (false);
		}
		lastKnownHp = playerContr.HP;
	}
}
