﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class DisguiseItem : MonoBehaviour, IBeginDragHandler,
IDragHandler, IEndDragHandler, IPointerClickHandler{


	public enum TYPE
	{
		HEAD_TYPE,
		EYE_TYPE,
		MOUTH_TYPE,
		CHIN_TYPE
	};

	static bool pickup=false;
	
	public RectTransform bindSpot;
	public RectTransform showcaseBindSpot;
	public TYPE type;
	public Disguise enumNum;
	GameObject bound;

    public bool fake = false;
	
	// Use this for initialization

	public int GetEnum()
	{
		if (type == TYPE.HEAD_TYPE) {
			return (int)enumNum.head;
		}
		else if (type == TYPE.EYE_TYPE) {
			return (int)enumNum.eye;
		}else if (type == TYPE.MOUTH_TYPE) {
			return (int)enumNum.mouth;
		}else {
			return (int)enumNum.chin;
		}
	
	}

	void Awake()
	{
		this.gameObject.SetActive (false);
		if (!GameObject.FindObjectOfType<DisguiseCaseController> ().allDisguises.ContainsKey (enumNum)) {
			GameObject.FindObjectOfType<DisguiseCaseController> ().allDisguises.Add (enumNum, this);
		}
	}

	void Start () 
    {

	}

	void OnEnable()
	{
		//GameObject.FindObjectOfType<DisguiseCaseController> ().SpawnDisguise (this);
	}

	public void OnPointerClick (PointerEventData eventData)
	{
		Debug.Log ("Clicked");
	}

	public void OnBeginDrag (PointerEventData eventData)
	{
		if (bound) {
			bound.GetComponent<DropPoint> ().Clear ();
			bound = null;
		}
		pickup = true;
		GetComponent<CanvasGroup> ().blocksRaycasts = false;
	}

	public void OnDrag (PointerEventData eventData)
	{
		this.transform.position += new Vector3 (eventData.delta.x,
		                                       eventData.delta.y,
		                                       0);
	}

	public void OnEndDrag (PointerEventData eventData)
	{
		pickup = false;
        if (!fake)
        {
            GetComponent<CanvasGroup>().blocksRaycasts = true;
        }
	}

	public void Bind(GameObject g)
	{
		bound = g;
	}

	public void Update()
	{
		if (pickup) {
			GetComponent<CanvasGroup> ().blocksRaycasts = false;
		}
		else{
            if (!fake)
            {
                GetComponent<CanvasGroup>().blocksRaycasts = true;
            }

		}
	}

}
