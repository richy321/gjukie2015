﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FadeManipulator : MonoBehaviour, IPointerClickHandler{

	public Image image;
	public float fadeSpeed = 0.8f;
	public int fadeDir = -1;

	bool firstFading;
	bool secondFading;

	void Start()
	{
		GetComponent<CanvasGroup> ().alpha = 1;
		image.GetComponent<CanvasGroup> ().alpha = 0;
	}

	void OnGUI()
	{

	}

	private IEnumerator StartFading()
	{
		CanvasGroup cg = GetComponent<CanvasGroup> ();

			while (cg.alpha > 0) {
				cg.alpha += fadeDir * fadeSpeed * Time.deltaTime;
				cg.alpha = Mathf.Clamp01 (cg.alpha);
				yield return null;
			}
		
		cg = image.GetComponent<CanvasGroup> ();
		while (cg.alpha <= 1.0f) {
			cg.alpha += - fadeDir * fadeSpeed * Time.deltaTime;
			cg.alpha = Mathf.Clamp01 (cg.alpha);
			yield return null;
		}

	}

	#region IPointerClickHandler implementation

	void IPointerClickHandler.OnPointerClick (PointerEventData eventData)
	{
		StartCoroutine(StartFading());
		//fade.enabled = true;
	}

	#endregion
}
