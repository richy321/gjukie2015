﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
public class DisguiseCaseAnimator : MonoBehaviour, IPointerClickHandler{


	public RectTransform upPos;
	public RectTransform downPos;

	public float timeTillArrival=1;
	static float t=0;
	static bool traveling=false;
	static bool arrived = true;
	// Use this for initialization
	void Awake () {
		arrived = true;
		GameObject.FindObjectOfType<DisguiseCaseController> ().lidDown = arrived;
	}
	
	// Update is called once per frame
	void Update () {
		if (traveling) {
			Vector3 newPos;
			t+=Time.deltaTime;
			if(!arrived)
			{
				newPos=Vector2.Lerp(upPos.position,downPos.position,t/timeTillArrival);
			}
			else{
				newPos=Vector2.Lerp(downPos.position,upPos.position,t/timeTillArrival);
			}
			this.gameObject.transform.parent.transform.position=newPos;
			if(t>=timeTillArrival)
			{
				arrived=!arrived;
				GameObject.FindObjectOfType<DisguiseCaseController> ().lidDown = arrived;
				traveling=false;
				t=0;
			}
		}
	}

	public void OnPointerClick (PointerEventData eventData)
	{
		GameObject.FindObjectOfType<SoundManager> ().Play ("Latch");
		GameObject.FindObjectOfType<DisguiseCaseController> ().lidDown = !arrived;
		traveling = true;
	}
}
