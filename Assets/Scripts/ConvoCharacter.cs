﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConvoCharacter : ScriptableObject 
{
    public List<ConvoNode> GenericConvoNode = new List<ConvoNode>();
    public string CharacterName;
    public GameObject characterSprite;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

}
