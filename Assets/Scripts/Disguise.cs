﻿using UnityEngine;
using System.Collections;

public enum HEAD_DIS
{
    NONE,
    GREEN_HAIR,
	ELVIS_HAIR,
	FEDORA,
	BEST_HAIR_EVER,
	BALD
}

public enum EYE_DIS
{
    NONE,
	CAT_MASK,
	DARK_GLASSES
}
public enum MOUTH_DIS
{
    NONE,
	DEBUG_STASH,
	CIGUAR,
}
public enum CHIN_DIS
{
    NONE,
	BOWTIE,
	PINK_BEARD,
	DOCTOR_THING,
}
[System.Serializable]
public class Disguise
{
	public Disguise(int h, int e, int m, int c)
	{
		head = (HEAD_DIS)h;
		eye = (EYE_DIS)e;
		mouth = (MOUTH_DIS)m;
		chin = (CHIN_DIS)c;

	}
	public override bool Equals(object obj)
	{
		return Compare ((Disguise)obj);
	}
	public override int GetHashCode()
	{
		int hash = 1337;
		hash = (hash * 7) + (int)head;
		hash = (hash * 7) + (int)eye;
		hash = (hash * 7) + (int)mouth;
		hash = (hash * 7) + (int)chin;
		return hash;

	}
	public bool IsNoDisguise()
	{
		return (head == HEAD_DIS.NONE &&
			eye == EYE_DIS.NONE &&
			mouth == MOUTH_DIS.NONE &&
			chin == CHIN_DIS.NONE);

	}
	public Disguise()
	{
		head = HEAD_DIS.NONE;
		eye = EYE_DIS.NONE;
		mouth = MOUTH_DIS.NONE;
		chin = CHIN_DIS.NONE;

	}
	public bool Compare(Disguise d)
	{
		return (d.head == head &&
			d.chin == chin &&
			d.eye == eye &&
			d.mouth == mouth);

	}
	public int debugView_head {get {return (int) head; }}
    public HEAD_DIS head;
	public int debugView_eye {get {return (int) eye; }}
    public EYE_DIS eye;
	public int debugView_mouth {get {return (int) mouth; }}
    public MOUTH_DIS mouth;
	public int debugView_chin {get {return (int) chin; }}
    public CHIN_DIS chin;
}
