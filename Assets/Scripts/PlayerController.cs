﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour {

	public Disguise CurrentDisguise=new Disguise();
	public DisguiseCaseController controller;

    public int HP = 2;

	public void UnlockDisguise(Disguise newDis)
	{
        controller.SpawnDisguise(newDis);
	}

	// Use this for initialization
	void Start()
    {
		HP = 2;
        if(!controller)
            controller = GameObject.FindObjectOfType<DisguiseCaseController>();
	}
	
	// Update is called once per frame
	void Update () 
    {   
		
	}

    public void TakeDamage()
    {
        HP--;
		FindObjectOfType<SoundManager> ().Play ("Alarm");
        if (HP < 0)
        {
            Application.LoadLevel(Application.loadedLevel);
        }
    }
}
