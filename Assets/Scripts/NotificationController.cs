﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class NotificationController : MonoBehaviour 
{
    public Queue<string> messageQueue = new Queue<string>();

    public float displayTimeLimit = 1.5f;
    public float currentDisplayTime = 0.0f;
    public Text notificationTextUI;
    public bool notificationActive = false;
    public float slideAmountMax = 75.0f;
    public float originalY;
    public float slideDelta = 1.5f;

    bool initialised = false;
	// Use this for initialization
	void Start () 
    {
        if (!notificationTextUI)
            Debug.LogError("No Notification UI Text assigned to NotificationController");

        originalY = notificationTextUI.transform.position.y;
	}


    public enum SlideMode
    {
        SlideUp,
        SlideDown,
        None
    }
    SlideMode slideMode = SlideMode.None;

	// Update is called once per frame
	void Update () 
    {
        RectTransform trans = (RectTransform)notificationTextUI.transform;

        if (!initialised)
        {
            originalY = notificationTextUI.transform.position.y;
            initialised = true;
        }

        if (notificationActive)
        {
            
            if (slideMode == SlideMode.SlideUp)
            {
                if (notificationTextUI.transform.position.y >= originalY + slideAmountMax)
                    slideMode = SlideMode.None;
                else
                    notificationTextUI.transform.Translate(0.0f, slideDelta, 0.0f);


            }
            else if (slideMode == SlideMode.SlideDown)
            {
                if (notificationTextUI.transform.position.y <= originalY)
                {
                    if (messageQueue.Count > 0)
                    {
                        notificationTextUI.text = messageQueue.Dequeue();
                        slideMode = SlideMode.SlideUp;
                    }
                    else
                    {
                        notificationTextUI.text = "";
                        notificationActive = false;
                        slideMode = SlideMode.None;
                    }
                }
                else
                    notificationTextUI.transform.Translate(0.0f, -slideDelta, 0.0f);
            }

            if (slideMode == SlideMode.None)
            {
                currentDisplayTime += Time.deltaTime;

                if (currentDisplayTime > displayTimeLimit)
                {
                    slideMode = SlideMode.SlideDown;
                    currentDisplayTime = 0.0f;
                }
            }
        }
        else
        {
            if (messageQueue.Count > 0)
            {
                notificationTextUI.text = messageQueue.Dequeue();
                notificationActive = true;
                slideMode = SlideMode.SlideUp;
            }
        }
	}

    public void AddNotificationToQueue(string notificationMessage)
    {
        messageQueue.Enqueue(notificationMessage);
    }

    public void AddTestNotification()
    {
        AddNotificationToQueue("Test");
    }
}
