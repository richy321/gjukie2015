﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class ConvoItemButton : MonoBehaviour, IPointerClickHandler
{
    public ConvoNode node;
    public LevelController levelController;
    public PlayerController playerController;
    public bool isExitConvo = false;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public void OnPointerClick(PointerEventData eventData)
    {
		int i = Random.Range (0, levelController.maleSpeakNoises.Count);

		GameObject.FindObjectOfType<SoundManager> ().Play (levelController.maleSpeakNoises [i]);
	
		Debug.Log (levelController.textIsLeft);

        if (!node.unlockDisguise.IsNoDisguise ()) {
			GameObject.FindObjectOfType<NotificationController> ().AddNotificationToQueue ("Open your disguise case at the bottom!!");
			GameObject.FindObjectOfType<SoundManager> ().Play ("Scissors");
			playerController.UnlockDisguise (node.unlockDisguise);
		}

        if (node.endOfLevel)
            levelController.LoadNextLevel();

        if (node.decrementHealth)
            playerController.TakeDamage();

        if (isExitConvo)
        {
            levelController.SwitchScreen(LevelController.ScreenType.CharacterSelect);
			levelController.CurrentLevel.currentConvoCharacter.characterSprite.transform.position= new Vector3(10000,10000,0);
			return;
        }
        if (node.children.Count > 0)
        {
            levelController.CurrentLevel.ClearUIConvoOptions();
            levelController.CurrentLevel.PopulateUIConvoOptionsFromNodes(node.children, playerController.CurrentDisguise);
			levelController.textIsLeft = node.children[0].Speaker==ConvoNode.SpeakerType.Player;
        }
        else
        {
            //levelController.CurrentLevel.AddConvoNodesToUIPanel(levelController.CurrentLevel.currentConvoCharacter, playerController.CurrentDisguise);
            levelController.SwitchScreen(LevelController.ScreenType.CharacterSelect);
            levelController.CurrentLevel.currentConvoCharacter.characterSprite.transform.position = new Vector3(10000, 10000, 0);
            return;

        }
    }
}
