﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class CharacterButton : MonoBehaviour, IPointerClickHandler 
{    
    public LevelController levelController;
    public PlayerController playerController;
    public ConvoCharacter character;

	// Use this for initialization
	void Start () 
    {
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public void OnPointerClick(PointerEventData eventData)
    {
        levelController.CurrentLevel.AddConvoNodesToUIPanel(character, playerController.CurrentDisguise);
    	levelController.SwitchScreen(LevelController.ScreenType.Conversation);
		character.characterSprite.transform.position = GameObject.Find (character.characterSprite.name + "Bind").transform.position;
		if (character.GenericConvoNode [0].Speaker == ConvoNode.SpeakerType.Player) {
			levelController.textIsLeft = true;
		} else {
			levelController.textIsLeft= false;
		}
	}
}
