﻿using UnityEngine;
using System.Collections;

public class DisguiseImageUpdater : MonoBehaviour {

	RectTransform headTran;
	RectTransform eyeTran;
	RectTransform mouthTran;
	RectTransform chinTran;

	DisguiseCaseController caseController;

	bool oldLidState;

	 DisguiseItem h;
	public DisguiseItem e;
	DisguiseItem m;
	DisguiseItem c;


    void OnEnable()
    {
        if (caseController)
        {
            DisguiseSelf();
        }
    }

	void Awake()
	{
		headTran = (RectTransform)(gameObject.transform.FindChild ("Head").transform);
		if (!headTran) {
			Debug.LogError("Cant find head slot on player head");
		}
		eyeTran = (RectTransform)(gameObject.transform.FindChild ("Eye").transform);
		if (!eyeTran) {
			Debug.LogError("Cant find head slot on player eye");
		}
		mouthTran = (RectTransform)(gameObject.transform.FindChild ("Mouth"));
		if (!mouthTran) {
			Debug.LogError("Cant find head slot on player mouth");
		}
		chinTran = (RectTransform)(gameObject.transform.FindChild ("Chin"));
		if (!chinTran) {
			Debug.LogError("Cant find head slot on player chin");
		}



	}
	// Use this for initialization
	void Start () {
		caseController = GameObject.FindObjectOfType<DisguiseCaseController> ();
		if (!caseController) {
			Debug.LogError("Cant find Disguise Case controller");
		}
		oldLidState = caseController.lidDown;
	}

	void DisguiseSelf()
	{
		Debug.Log ("Disguised Self");
		Disguise d = new Disguise (caseController.currentItems [0],
		                        caseController.currentItems [1],
		                        caseController.currentItems [2],
		                        caseController.currentItems [3]);

		if(h)
		{
			GameObject.Destroy(h.gameObject);
		}
		if(e)
		{
			GameObject.Destroy(e.gameObject);
		}
		if(m)
		{
			GameObject.Destroy(m.gameObject);
		}if(c)
		{
			GameObject.Destroy(c.gameObject);
		}
		if (!d.IsNoDisguise()) {
			Disguise head=new Disguise((int)d.head,0,0,0);
			Disguise eye=new Disguise(0,(int)d.eye,0,0);
			Disguise mouth=new Disguise(0,0,(int)d.mouth,0);
			Disguise chin=new Disguise(0,0,0,(int)d.chin);
		



			if(caseController.allActiveDisguises.ContainsKey(head))
			{
				h=EquipDisguise(caseController.allActiveDisguises[head],headTran);
			}
			if(caseController.allActiveDisguises.ContainsKey(mouth))
			{
				m=EquipDisguise(caseController.allActiveDisguises[mouth],mouthTran);

			}
			if(caseController.allActiveDisguises.ContainsKey(eye))
			{
				e=EquipDisguise(caseController.allActiveDisguises[eye],eyeTran);
			}
			if(caseController.allActiveDisguises.ContainsKey(chin))
			{
				c=EquipDisguise(caseController.allActiveDisguises[chin],chinTran);
			}
		}
	}

	DisguiseItem EquipDisguise(DisguiseItem item, RectTransform target)
	{
		DisguiseItem copy=GameObject.Instantiate(item);
		RectTransform r = copy.GetComponent<RectTransform> ();
		RectTransform parent = item.GetComponent<RectTransform> ();
		r.SetParent(this.transform);
		r.position=target.position;
		if (item.showcaseBindSpot) {
			r.position=item.showcaseBindSpot.position;
		}
		r.localScale = parent.localScale;
		copy.gameObject.SetActive (true);
        copy.fake = true;
		copy.GetComponent<CanvasGroup> ().blocksRaycasts = false;
		copy.GetComponent<CanvasGroup> ().interactable = false;
			return copy;
	}
	
	// Update is called once per frame
	void Update () {
		if (caseController.lidDown == true &&
			caseController.lidDown != oldLidState) {
			DisguiseSelf();
		}
		oldLidState=caseController.lidDown;
	}
}
