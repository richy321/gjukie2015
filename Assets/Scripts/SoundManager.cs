﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum SoundsType{Ambient = 0, Effect = 1, Conversation = 2};

[System.Serializable]
public class CustomSound
{
	public string Name;
	public SoundsType Type;
	public AudioClip Clip;
}

public class SoundManager : MonoBehaviour {
	
	AudioSource[] aSources; 
	
	bool sourceFound;
	
	public CustomSound[] sounds;
	
	// Use this for initialization
	void Start () {
		//
		aSources = GetComponents<AudioSource>();
		//Zero is always ambient and it should loop
		aSources [0].loop = true;
		aSources [0].clip = sounds [0].Clip;
		aSources [0].Play ();
	}
	
	// Update is called once per frame
	void Update () {
		//StartCoroutine( DebugCoroutine() );
	}

	void Awake(){

	}

	private IEnumerator DebugCoroutine()
	{
		yield return new WaitForSeconds( 5.0f );
		aSources [0].Play ();
		// process pre-yield
		yield return new WaitForSeconds( 20.0f );
		// process post-yield
		Play ("AAA");
	}
	
	public void Play(string clipName)
	{
		for (int i = 0; i < sounds.Length; i++) {
			if (clipName == sounds [i].Name) {
				int intEnum = (int)sounds [i].Type;
				aSources [intEnum].Stop ();
				aSources [intEnum].clip = sounds [i].Clip;
				aSources [intEnum].Play ();
				sourceFound = true;
				break;
			}
		}
		
		if (sourceFound == false) 
		{
			Debug.Log("Audio Clip has not been found");
		}
		
		sourceFound = false;
	}
}

