﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class Level : MonoBehaviour 
{
    public string LevelName;
    public uint LevelNumber;
    public List<ConvoCharacter> characters = new List<ConvoCharacter>();   
    public Level NextLevel;
    public Sprite BackgroundImage;

    public GameObject CharacterUIPanel;
    public GameObject ConvoUIPanel;

    public GameObject CharacterUIButton;
    public GameObject ConvoTextItemPrefab;

    public GameObject Character1ConvoGO;
    public GameObject Character2ConvoGO;
    public GameObject Character3ConvoGO;
    public GameObject Character4ConvoGO;

	public Image DLCBanner;

    public ConvoNode back;

    //level 1 names
    public const string CodeNameSpyName = "Codename Spy";
    public const string AmbasidorName = "AmBADistor";
    public const string CatManSpyName = "Cat Man";

    //level 2 names
    public const string DragonName = "Dragon";
    public const string HulaGirlName = "Hula Girl";
    public const string AstronaughtName = "Atronaught";
    public const string RocketSurgeonName = "Rocket Surgeon";

    //level 3 names
    //public const string CatManSpyName = "Cat Man";
    //public const string CatManSpyName = "Cat Man";
    //public const string CatManSpyName = "Cat Man";
    //public const string CatManSpyName = "Cat Man";


    PlayerController playerController;
    LevelController levelController;
    DisguiseCaseController disguiseController;

    bool disguiseCasePreviousLidDown = true;

    public ConvoCharacter currentConvoCharacter;

	// Use this for initialization
	void Start() 
    {
        if (!CharacterUIPanel)
            CharacterUIPanel = GameObject.Find("ConvoCharacterSelect");
        if(!ConvoUIPanel)
            ConvoUIPanel= GameObject.Find("ConvoPanel");
        
        if (!playerController)
            playerController = GameObject.FindObjectOfType<PlayerController>();
        if (!levelController)
            levelController = GameObject.FindObjectOfType<LevelController>();
        if (!disguiseController)
            disguiseController = GameObject.FindObjectOfType<DisguiseCaseController>();

        disguiseCasePreviousLidDown = disguiseController.lidDown;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (disguiseController.lidDown != disguiseCasePreviousLidDown && levelController.CurrentLevel.currentConvoCharacter)
            levelController.CurrentLevel.AddConvoNodesToUIPanel(levelController.CurrentLevel.currentConvoCharacter, playerController.CurrentDisguise);

       disguiseCasePreviousLidDown = disguiseController.lidDown;
	}  

    public void Initialise()
    {
        switch (LevelNumber)
        {
            case 1:
                InitialiseLevel1();
                break;
            case 2:
                InitialiseLevel2();
                break;
            case 3:
                InitialiseLevel3();
                break;
        }
    }

    void InitialiseLevel1()
    {
        characters.Clear();
        ClearCharactersUIPanel();
        GameObject.Find("Background").GetComponent<Image>().sprite = BackgroundImage;

        ConvoCharacter codenameSPY = ScriptableObject.CreateInstance<ConvoCharacter>();
        codenameSPY.CharacterName = CodeNameSpyName;
		codenameSPY.characterSprite = GameObject.Find ("Commander");
        codenameSPY.GenericConvoNode = LoadConvoNodesFromHierarchy(Character1ConvoGO);
        characters.Add(codenameSPY);

        ConvoCharacter ambasidor = ScriptableObject.CreateInstance<ConvoCharacter>();
        ambasidor.CharacterName = AmbasidorName;
		ambasidor.characterSprite = GameObject.Find ("AmBadIsdor");
        characters.Add(ambasidor);
        ambasidor.GenericConvoNode = LoadConvoNodesFromHierarchy(Character2ConvoGO);

        ConvoCharacter CatManSpy = ScriptableObject.CreateInstance<ConvoCharacter>();
        CatManSpy.CharacterName = CatManSpyName;
		CatManSpy.characterSprite = GameObject.Find ("Feline Fetal");
        characters.Add(CatManSpy);
        CatManSpy.GenericConvoNode = LoadConvoNodesFromHierarchy(Character3ConvoGO);

        AddCharactersToUIPanel();

        //Loading level audio
        GameObject.Find("AudioManager").SendMessage("Play", "Embassy");

		DLCBanner.gameObject.SetActive (false);

        Debug.Log("InitialiseLevel1");
    }

    void InitialiseLevel2()
    {
        characters.Clear();
        ClearCharactersUIPanel();
        GameObject.Find("Background").GetComponent<Image>().sprite = BackgroundImage;

        ConvoCharacter dragon = ScriptableObject.CreateInstance<ConvoCharacter>();
        dragon.CharacterName = DragonName;
        dragon.characterSprite = GameObject.Find("Commander");
        dragon.GenericConvoNode = LoadConvoNodesFromHierarchy(Character1ConvoGO);
        characters.Add(dragon);

        ConvoCharacter hulaGirl = ScriptableObject.CreateInstance<ConvoCharacter>();
        hulaGirl.CharacterName = HulaGirlName;
        hulaGirl.characterSprite = GameObject.Find("Commander");
        hulaGirl.GenericConvoNode = LoadConvoNodesFromHierarchy(Character2ConvoGO);
        characters.Add(hulaGirl);

        ConvoCharacter astronaught = ScriptableObject.CreateInstance<ConvoCharacter>();
        astronaught.CharacterName = AstronaughtName;
        astronaught.characterSprite = GameObject.Find("Commander");
        astronaught.GenericConvoNode = LoadConvoNodesFromHierarchy(Character3ConvoGO);
        characters.Add(astronaught);

        ConvoCharacter rocketSurgeon = ScriptableObject.CreateInstance<ConvoCharacter>();
        rocketSurgeon.CharacterName = RocketSurgeonName;
        rocketSurgeon.characterSprite = GameObject.Find("Commander");
        rocketSurgeon.GenericConvoNode = LoadConvoNodesFromHierarchy(Character4ConvoGO);
        characters.Add(rocketSurgeon);

        AddCharactersToUIPanel();

        //Loading level audio
        GameObject.Find("AudioManager").SendMessage("Play", "AtTheShore");

		DLCBanner.gameObject.SetActive (true);

        Debug.Log("InitialiseLevel2");
    }

    void InitialiseLevel3()
    {
        characters.Clear();
        ClearCharactersUIPanel();

        AddCharactersToUIPanel();

        //Loading level audio
        GameObject.Find("AudioManager").SendMessage("Play", "Embassy");

        Debug.Log("InitialiseLevel3");
    }

    void ClearCharactersUIPanel()
    {
        for (int i = 0; i < CharacterUIPanel.transform.childCount; i++)
        {
            GameObject.Destroy(CharacterUIPanel.transform.GetChild(i).gameObject);
        }
    }

    void AddCharactersToUIPanel()
    {
        foreach (var entry in characters)
        {
            GameObject characterSelectionButton = GameObject.Instantiate(CharacterUIButton);

            Text textBox = characterSelectionButton.transform.FindChild("CharacterSelectText").GetComponent<Text>();
            textBox.text = entry.CharacterName;

            Image imageBox = characterSelectionButton.transform.FindChild("CharacterSelectImage").GetComponent<Image>();
			imageBox.sprite = entry.characterSprite.GetComponent<Image>().sprite;

            characterSelectionButton.transform.SetParent(CharacterUIPanel.transform);

            CharacterButton charButtonScript = characterSelectionButton.GetComponent<CharacterButton>();
            charButtonScript.levelController = levelController;
            charButtonScript.playerController = playerController;
            charButtonScript.character = entry;
        }
    }

    public void AddConvoNodesToUIPanel(ConvoCharacter character, Disguise disguise)
    {
        currentConvoCharacter = character;
        ClearUIConvoOptions();
        PopulateUIConvoOptionsFromNodes(character.GenericConvoNode, disguise);

        AddUIConvoItemFromNode(back, true);
    }

    public void ClearUIConvoOptions()
    {
        for (int i = 0; i < ConvoUIPanel.transform.childCount; i++)
        {
            GameObject.Destroy(ConvoUIPanel.transform.GetChild(i).gameObject);
        }
    }

    public void PopulateUIConvoOptionsFromNodes(List<ConvoNode> nodes, Disguise currentDisguise)
    {
        foreach (var node in nodes)
        {
            if(node.disguiseRequired.IsNoDisguise() || node.disguiseRequired.Compare(currentDisguise))
                AddUIConvoItemFromNode(node,false);
        }
    }

    public void AddUIConvoItemFromNode(ConvoNode node, bool isExitConvo)
    {
        GameObject textItem = GameObject.Instantiate(ConvoTextItemPrefab);
   
        textItem.transform.SetParent(ConvoUIPanel.transform);
        ConvoItemButton buttonScript = textItem.GetComponent<ConvoItemButton>();
        buttonScript.playerController = playerController;
        buttonScript.levelController = levelController;
        buttonScript.node = node;
        buttonScript.isExitConvo = isExitConvo;

        Text text = textItem.GetComponent<Text>();
        text.text = node.Text;
    }

    public List<ConvoNode>LoadConvoNodesFromHierarchy(GameObject rootGO)
    {
        if (!rootGO)
        {
            Debug.LogError("No rootGO for LoadConvoNodesFromHierarachy");
            return null;
        }

        List<ConvoNode> rval = new List<ConvoNode>();
        for (int i = 0; i < rootGO.transform.childCount; i++)
        {
            ConvoNode convoRoot = rootGO.transform.GetChild(i).GetComponent<ConvoNode>();
            AddChildrenToNode(convoRoot, rootGO.transform.GetChild(i).gameObject);
            rval.Add(convoRoot);
        }

        return rval;
    }

    public void AddChildrenToNode(ConvoNode parentConvoNode, GameObject GO)
    {
        if (parentConvoNode.jumpNode)
            parentConvoNode.children.Add(parentConvoNode.jumpNode);

        for (int i = 0; i < GO.transform.childCount; i++)
        {
            GameObject entryGO = GO.transform.GetChild(i).gameObject;
            ConvoNode node = entryGO.GetComponent<ConvoNode>();

            parentConvoNode.children.Add(node);
            AddChildrenToNode(node, entryGO);
        }
    }
}
