﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class DisguiseCaseController : MonoBehaviour {


	public PlayerController pc;
	public GameObject polaroidArea;

	public int[] currentItems;
	[HideInInspector]
	public bool lidDown;

	public Dictionary<Disguise,DisguiseItem> allDisguises =new Dictionary<Disguise,DisguiseItem>();
	public Dictionary<Disguise,DisguiseItem> allActiveDisguises = new Dictionary<Disguise,DisguiseItem> ();


	public List<DisguiseItem> startingItems = new List<DisguiseItem> ();

	public List<Disguise> disguiseKeys;
	public List<GameObject> polaroidToUnlock;

	public RectTransform spawnArea;

	// Use this for initialization
	void Start () {
		currentItems = new int[4];

		if (!pc) 
        {
			pc=GameObject.FindObjectOfType<PlayerController>();
			if(!pc)
			{
				Debug.Log("No PlayerController in the scene");
			}
		}

		for(int i=0;i<startingItems.Count;++i)
		{
			SpawnDisguise(startingItems[i]);

		}
	}

	Vector2 GetSpawnPos()
	{
		return new Vector2 (Random.Range ((float)(spawnArea.position.x - spawnArea.rect.width * 0.5),
		                                (float)(spawnArea.position.x + spawnArea.rect.width * 0.5)),
		                   Random.Range ((float)(spawnArea.position.y - spawnArea.rect.height * 0.5),
		             (float)(spawnArea.position.y + spawnArea.rect.height * 0.5)));
	}
	public void SpawnDisguise(Disguise it)
	{
		Disguise head=new Disguise((int)it.head,0,0,0);
		Disguise eye=new Disguise(0,(int)it.eye,0,0);
		Disguise mouth=new Disguise(0,0,(int)it.mouth,0);
		Disguise chin=new Disguise(0,0,0,(int)it.chin);



		if (allDisguises.ContainsKey (head)) {
			SpawnDisguise(allDisguises[head]);
		}
		if (allDisguises.ContainsKey (eye)) {
			SpawnDisguise(allDisguises[eye]);
		}
		if (allDisguises.ContainsKey (mouth)) {
			SpawnDisguise(allDisguises[mouth]);
		}
		if (allDisguises.ContainsKey (chin)) {
			SpawnDisguise(allDisguises[chin]);
		}
	}

	public void SpawnDisguise(DisguiseItem it)
	{
		if (it) {
			if (it.gameObject.activeSelf != true) {
				it.gameObject.SetActive (true);
			}
			it.transform.position = GetSpawnPos ();
			if (!allActiveDisguises.ContainsKey (it.enumNum)) {
				allActiveDisguises.Add (it.enumNum, it);
			}
		}
	}

	void Update()
	{
		pc.CurrentDisguise.head = (HEAD_DIS)currentItems [0];
		pc.CurrentDisguise.eye = (EYE_DIS)currentItems [1];
		pc.CurrentDisguise.mouth = (MOUTH_DIS)currentItems [2];
		pc.CurrentDisguise.chin = (CHIN_DIS)currentItems [3];
		for(int i=0;i<disguiseKeys.Count;++i)
		{
			if(pc.CurrentDisguise.Compare(disguiseKeys[i]))
			{
				if(polaroidToUnlock[i]&&polaroidArea)
				{
					FindObjectOfType<SoundManager>().Play("Win");
					polaroidToUnlock[i].SetActive(true);
					polaroidToUnlock[i].transform.SetParent(polaroidArea.transform);
					polaroidToUnlock[i]=null;
				}
			}

		}
	}

}
