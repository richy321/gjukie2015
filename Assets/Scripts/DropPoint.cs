﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class DropPoint : MonoBehaviour, IDropHandler{

	public static int bounceDistance=4;
	public int type;

	public GameObject boundObject;



	public void OnDrop(PointerEventData data)
	{
		GameObject dragObj = (GameObject)data.pointerDrag;
		if ((int)dragObj.GetComponent<DisguiseItem> ().type == type) {
			if(!boundObject)
			{
				Grab (dragObj);
			}
		} else {
			Bounce(dragObj);
		}
	}

	public void Clear()
	{
		boundObject = null;
		RegisterDisguise (0);
	}

	void Bounce(GameObject dragObj)
	{
		dragObj.transform.position = transform.position+(dragObj.transform.position- transform.position)
			*bounceDistance;
	}

	void Grab(GameObject dragObj)
	{
		if (dragObj.GetComponent<DisguiseItem> ().bindSpot) {
			dragObj.transform.position=dragObj.GetComponent<DisguiseItem>().bindSpot.position;
		} else {
			dragObj.transform.position = transform.position;
		}
		RegisterDisguise(dragObj.GetComponent<DisguiseItem>().GetEnum());
		dragObj.GetComponent<DisguiseItem> ().Bind (this.gameObject);
		boundObject=dragObj;
	}

	void RegisterDisguise(int enumNum)
	{
		Debug.Log (enumNum);
				GameObject g = GameObject.FindGameObjectWithTag ("DisguiseController");
				g.GetComponent<DisguiseCaseController> ().currentItems [type] = enumNum;
	}
}
